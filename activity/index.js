let num = prompt("Give me a number.");
console.log("The number you provided is "+num+".")
for (num;num>=0;num--) {
	if (num <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
	if (num%10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	if (num%5 === 0) {
		console.log(num);
		continue;
	}
}

let maryPoppins = "supercalifragilisticexpialidocious";
let charSuper = [];

for (let i = 0; i < maryPoppins.length; i++) {
	let charName = maryPoppins[i].toLowerCase();
	if (charName === "a" ||
		charName === "e" ||
		charName === "i" ||
		charName === "o" ||
		charName === "u") {
		continue;	
	}
	else {
		charSuper = charSuper+charName;
	}
}
console.log(charSuper);