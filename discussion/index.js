console.log("Hello World");

let count = 5;
	console.log("*Initial Count Value: "+count);

// WHILE LOOP

while (count !== 0) {
	console.log("While Loop Result: "+count);
	count--;
}

/*MINIACTIVITY*/
/*
	reassign count to 0
	create a while loop that checks the value of the count
		as long as value is <= 10
		log
		++
*/

count = 0;
	console.log("*Initial Count Value: "+count);
while (count <= 10) {
	console.log("While Loop Result: "+count);
	count++;
}

// DO-WHILE LOOP

let number = Number(prompt("Give me a number."));
	console.log("*Initial Number Value: "+number);
do {
	console.log("Do-While Loop:"+number);

	number++;
}
while (number<10);

// FOR LOOP

for (let i = 0; i <= 20; i++) {
	if (i===0){
	console.log("*Initial 'i' Value: "+i);
	}
	console.log("For Loop Result: "+i);
}

// USING STRINGS IN FOR LOOP

let myString = "alex";
console.log(myString);

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

/*MINIACTIVITY*/
/*
	Create a let "myName" value is name
	for loop, log each character but always lowercase

*/

let myName = "jygsgoco";
console.log(myName);

for (let x = 0; x < myName.length; x++) {
	let charName = myName[x].toLowerCase();
	if (charName === "a" || charName === "e" || charName === "i" || charName === "o" || charName === "u"){
	console.log(3);
	}
	else {
	console.log(charName);
	}
}


// CONTINUE AND BREAK STATEMENTS

for (let i = 0; i <= 20; i++) {
	if (i%2===0){
		continue;
	}
	if (i>10){
		break;
	}
	console.log("Continue and Break Result: "+i);
}

let name = "alexandro"

/*
	MINIACTIVITY
		create for loop with continue and break statements
			try to log in the console each letter of the name with a catch
			if the letter is a, continue
			if the letter is d, break

		send the output in the google chat
*/

for (let i = 0; i < name.length; i++) {
	let charName = name[i].toLowerCase();
	if (charName === "a"){
		continue;
	}
	console.log(charName);
	if (charName === "d"){
		break;
	}
}

for (let x = 0; x <= 3; x++) {
	for (let y = 0; y <= x; y++) {
		console.log("x: "+ x +" y: "+y);
	}
}